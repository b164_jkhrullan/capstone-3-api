const express = require("express");

const router = express.Router();

const OrderController = require("../controllers/orderController");

const auth = require('../auth');

router.post("/create", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin){
		res.send(false)
	} else {
		OrderController.createOrder(req.body).then(result => res.send(result));
	}
})

router.get("/", (req, res) => {
	const user = auth.decode(req.headers.authorization)
	OrderController.getUserOrders(user.id).then(result => res.send(result));
})


router.get("/all", (req, res) => {

	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin){
		OrderController.getAllOrders().then(result => res.send(result));
	} else {
		res.send(false)
	}
	
});






















module.exports = router;