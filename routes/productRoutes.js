const express = require("express");

const router = express.Router();

const ProductController = require("../controllers/productController");

const auth = require('../auth')

router.get("/", (req, res) => {
	ProductController.fetchAllProducts().then(result => res.send(result));
})

router.get("/active", (req, res) => {
	ProductController.fetchActiveProducts().then(result => res.send(result));
})

router.post("/create", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin){
		ProductController.createProduct(req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

router.get("/:productId", (req, res) => {
	ProductController.fetchById(req.params.productId).then(result => res.send(result));
})

router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

router.put('/:productId/archive', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin){
		ProductController.archiveProduct(req.params.productId, req.body).then(result => res.send(result))
	} else {
		res.send(false);
	}
})



















module.exports = router;

