const Product = require("../models/Product");

module.exports.fetchAllProducts = () => {
	return Product.find({
	}); 

} 

module.exports.fetchById = (id) => {
	return Product.findById(id);
}

module.exports.fetchActiveProducts = () => {
	return Product.find({
		isActive: true
	})
}

module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imageUrl: reqBody.imageUrl,
		isActive: reqBody.isActive
	})

	return newProduct.save().then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.updateProduct = (productId, reqBody) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imageUrl: reqBody.imageUrl,
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(productId, updateProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	}
	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}







