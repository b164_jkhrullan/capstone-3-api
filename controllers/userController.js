const User = require("../models/User");

const bcrypt = require('bcrypt');

const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		if(result.length > 0){
			return true;
		} else {

			return false;
		}
	})
}



module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		type: reqBody.type
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})

}


module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)


			if(isPasswordCorrect){
			
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}

		};
	});
};


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";

		return result;
	})
}

module.exports.updateUser = (userId, reqBody) => {
	if(reqBody.password == null){
		reqBody.password = ""
	}
	let updateUser = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		type: reqBody.type
	};
	return User.findByIdAndUpdate(userId, updateUser).then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}




