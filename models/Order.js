const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required"]

	},

	product: {
		id: {
			type: String,
			required: [true, "productId is required"]
		},		
		name: {
			type: String,
			required: [true, "Name is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
	},
			

	status: {
		type: String,
		required: [true, "status is required"]
	}
})




module.exports = mongoose.model("Order", orderSchema)